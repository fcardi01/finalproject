-- Breakdown of Tables data

SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total 
from response 
group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) 
order by 1;

DAY            TOTAL
--------- ----------
20-JAN-15      31373
21-JAN-15      42911
22-JAN-15      42823
23-JAN-15      41909
24-JAN-15      32611
25-JAN-15      30762
26-JAN-15      48980
27-JAN-15      46360
28-JAN-15      46186



SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total 
from request 
group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) 
order by 1;

DAY            TOTAL
--------- ----------
20-JAN-15      44157
21-JAN-15      42366
22-JAN-15      42306
23-JAN-15      41395
24-JAN-15      32273
25-JAN-15      30476
26-JAN-15      48262
27-JAN-15      45654
28-JAN-15      38558




--- Cronjob cleaning the DB

delete  response where (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) <= trunc(sysdate-8);

delete  request where (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) <= trunc(sysdate-8);