#!/usr/local/bin/perl
#use strict;
use warnings;
use DBI;



# Connecting to the database. Take the SID from TNSNAMES.ORA.
# Here the DBD is loaded.
# $dbh will be the database handle - a variable through whic
# you connect to your database.

 

my $dbh = DBI->connect( "dbi:Oracle:main", "fcardi01", "bbkfranco" )  or die "Couldnt connect to database: $DBI::errstr";



# $sth is a statement handle - an environment for running an SQL statement.
my $sth = $dbh->prepare('select trunc(sysdate-900) from dual') 
    or die 'Couldnt prepare statement: ' . $dbh->errstr;

# Execute the SQL statement; don't print it yet
$sth->execute or warn 'Execute failed: ' . $dbh->errstr;

# This "loop" prints all the rows (actually just one, in this case)
while (my @row = $sth->fetchrow_array) {
    print "@row\n";
}



# gracefully disconnect from the database
$dbh->disconnect();

