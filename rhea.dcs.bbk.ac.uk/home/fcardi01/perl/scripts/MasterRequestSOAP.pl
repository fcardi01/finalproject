#!/usr/local/bin/perl
#use strict;
use warnings;
use DBI;

##########################################################
#                                                        #
# FRANCO CARDINALI | FCARDI01 | BSc Final Year Project   #
# Application Performances and Statistics Tool   - APST  #
# SCRIPT: MasterRequest.pl                               #
# This Script Will store Information about SOAP|GUI      #
# Requests only.   									     #
#                                                        #
##########################################################

#VARIABLES
################################################################################################################

#dbh CONTAINS DB CONNECTION CREDENTIALS  
my $dbh = DBI->connect( "dbi:Oracle:main", "fcardi01", "bbkfranco" )  or die "Couldnt connect to database: $DBI::errstr";
  

#file IS THE LOCATION OF THE FILE WHERE THE FTP JOB WILL DROP THE LOGS READY TO BE PROCESSED 
my $file ='/home/fcardi01/perl/logs/request/request.txt';

#line KEEPS THE COUNT OF HOW MANY RECORDS I HAVE INSERTED
my $line=0;

#################################################################################################################

# Printing On The Screen
print"Fetching Log File - Looking For SOAP Requests\n\n";


# Opening File Streaming
open my $fh, "<", $file or die "Could not open $file: $!";

# Processing Each Line
# REGEXP will store SOAP|GUI Requests only
while (<$fh>) {
    my @fields = m{^
        (\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:[\d,]+)
        \s (ERROR|INFO|DEBUG|WARN) \s\s
        \[SOAP\s[(]User:["](\w+)",\sThreadId:\s\d+\)] 
        \s com.metapack.common.PerformanceLogger \s \(PerformanceLogger\.java\:\d+\) \s - \s
        RetailerId:(\d+),
        UserId:(\d+),
        RemoteIp:([\d.]+),
        DurationMillis:([\d]+),
        DurationText:(?: [\d.]+ | \d+ \sminutes \s\d+ ) \sseconds,
        Url:SOAP/(.*)
    $}x

	
	 # Skipping Records That will not match the pattern
      or next;  

    # Storing Values into Variables to use in SQL inserts
	my ( $DATETIME_EVENT, $LOG_LEVEL, $LOGONID, $CLIENTID, $USERID, $IPADDRESS, $DURATION, $CLASSMETHOD ) = @fields;
	
	# PREPARING INSERT
	my $sth = $dbh->prepare('INSERT INTO REQUEST (DATETIME_EVENT,LOG_LEVEL,LOGONID, CLIENTID, USERID,IPADDRESS,DURATION,CLASSMETHOD)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?)')
	or die "Couldn't execute statement: " . $dbh->errstr;
	
	# EXECUTING INSERT
	$sth->execute( $DATETIME_EVENT, $LOG_LEVEL, $LOGONID, $CLIENTID, $USERID, $IPADDRESS, $DURATION, $CLASSMETHOD )
	or warn 'Execute failed: ' . $dbh->errstr;

	#Counter Variable For Inserted Rows
	$line= $line + 1;
}

# Printing On The Screen
print"Log File Scanned \n\nNow Disconnecting From DB \n\n";

# DISCONNECTING THE DB
$dbh->disconnect();

# Printing On The Screen
print"Total Line/s Inserted: $line\n ";