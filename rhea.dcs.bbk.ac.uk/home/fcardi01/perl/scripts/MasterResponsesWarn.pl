#!/usr/local/bin/perl
#use strict;
use warnings;
use DBI;

##########################################################
#                                                        #
# FRANCO CARDINALI | FCARDI01 | BSc Final Year Project   #
# Application Performances and Statistics Tool   - APST  #
# SCRIPT: MasterResponsesWarn.pl                         #
# This Script Will store Information about Warning       #
# responses only.   									 #
# VERSION 2.1	14 February 2015                         #
##########################################################

#VARIABLES
################################################################################################################
                                                                                                               
#dbh CONTAINS DB CONNECTION CREDENTIALS  
my $dbh = DBI->connect( "dbi:Oracle:main", "fcardi01", "bbkfranco" )  or die "Couldnt connect to database: $DBI::errstr";

#file IS THE LOCATION OF THE FILE WHERE THE FTP JOB WILL DROP THE LOGS READY TO BE PROCESSED 
my $file ='/home/fcardi01/perl/logs/responses/response.txt';

#line KEEPS THE COUNT OF HOW MANY RECORDS I HAVE INSERTED
my $line=0;

#################################################################################################################

# Printing On The Screen
print"Fetching Log File - Looking For Warnings\n\n";

# Opening File Streaming
open my $fh, "<", $file or die "Could not open $file: $!";



# Processing Each Line
# REGEXP will store Warning|SQL Responses only
while (<$fh>) {
	my @fields = m{^
        (\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:[\d,]+)
        \s (WARN) \s\s
        \[(?: SOAP | GUI )\s[(]User:["](\w+)",\sThreadId:\s\d+\)] 
        \s com.metapack.deliverymanager.dao.(\D+) \s \(\S+\.java\:\d+\) \s- \s
        \D+ \s
        \[((?: [\d.]+ \sseconds | \d+ \sminutes \s\d+ \sseconds ))]
        \s (.*)
    $}x
    

     # Skipping Records That will not match the pattern	
     or next;   

	# Storing Values into Variables to use in the SQL inserts below in the code
	my ( $DATETIME_EVENT, $LOG_LEVEL, $LOGONID, $SERVICE, $DURATIONSQL, $MESSAGE ) = @fields;
	
		# Brilliant: getting CLIENTID and USERID in the WHILE, reading each line and querying the CLIENT_USER table 
		my $sql = 'SELECT CLIENTID, USERID  FROM CLIENT_USER WHERE LOGONID  = ?';
		my $st = $dbh->prepare($sql);
		$st->execute("$LOGONID");
		my @row = $st->fetchrow_array;
	

	# PREPARING INSERT
	my $sth = $dbh->prepare('INSERT INTO RESPONSE (DATETIME_EVENT, LOG_LEVEL, LOGONID, CLIENTID , USERID , SERVICE, DURATIONSQL, MESSAGE)
	VALUES (?, ?, ?, ? ,?, ?, ?, ? )')
	or die "Couldn't execute statement: " . $dbh->errstr;

	# EXECUTING INSERT
	# $row[0] and  $row[1] are the values that correspond to CLIENTID and USERID
	$sth->execute( $DATETIME_EVENT,$LOG_LEVEL , $LOGONID, $row[0], $row[1],$SERVICE ,$DURATIONSQL,$MESSAGE )
	or warn 'Execute failed: ' . $dbh->errstr;

	#Counter Variable For Inserts
	$line= $line + 1;
}

# Printing On The Screen
print"Log File Scanned \n\nNow Disconnecting From DB \n\n";

# DISCONNECTING THE DB
$dbh->disconnect();

# Printing On The Screen
print"Potential Line/s  Inserted: $line\n ";