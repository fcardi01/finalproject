/*==================TABLE CLIENT======================*/

CREATE TABLE CLIENT
(
  CLIENTID             NUMBER                   NOT NULL,
  NAME                 VARCHAR2(255 BYTE)       NOT NULL
);

-- CLIENT PK
ALTER TABLE CLIENT ADD CONSTRAINT CLIENT_PK  PRIMARY KEY (CLIENTID);