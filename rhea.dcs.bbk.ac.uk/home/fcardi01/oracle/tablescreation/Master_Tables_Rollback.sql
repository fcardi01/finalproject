/*
==================MASTER ROLLBACK TABLE SCRIPT======================

FRANCO CARDINALI | FCARDI01 | BSc Final Year Project   
Application Performances and Statistics Tool   - APST  
SCRIPT: Master_Tables_Rollback.sql           
                  
Unistall all Tables Set-up and Data

*/

/*==================DELETING CONSTRAINTS ======================*/

-- FOREIGN KEYS
ALTER TABLE RESPONSE DROP CONSTRAINT FK_RESPONSE_USER_LOGON_CL;
ALTER TABLE RESPONSE DROP  CONSTRAINT FK_RESPONSET_CLIENT;
ALTER TABLE REQUEST DROP CONSTRAINT FK_REQUEST_USER_LOGON_CL;
ALTER TABLE REQUEST DROP CONSTRAINT FK_REQUEST_CLIENT;
ALTER TABLE CLIENT_USER DROP CONSTRAINT FK_CLIENT;

-- PRIMARY KEYS
ALTER TABLE CLIENT DROP CONSTRAINT CLIENT_PK;
ALTER TABLE CLIENT_USER DROP CONSTRAINT CLIENT_USER_LOGON_CL_PK;


/*==================ERASING ENTRIES FROM TABLES ===============*/

TRUNCATE TABLE CLIENT;
TRUNCATE TABLE CLIENT_USER;
TRUNCATE TABLE  REQUEST;
TRUNCATE TABLE  RESPONSE;

/*==================DELETING TABLES============================*/

DROP TABLE CLIENT;
DROP TABLE CLIENT_USER;
DROP TABLE REQUEST;
DROP TABLE RESPONSE;


COMMIT;


