<script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load('visualization', '1.1', {packages: ['line']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Day');
      data.addColumn('number', 'OptionsUsingBookingCode');
      data.addColumn('number', 'ConsignmentsByOrderReference');
      data.addColumn('number', 'createPaperworkForParcel');
   
   data.addRows([
			[1,	967,  4125 ,1251 ],
			[2,	842,  3607 ,1197 ],
			[2,	1059, 3378, 896  ],
			[4,	1272, 3243 ,905  ],
			[5,	1256, 2732 ,612  ],
			[6,	1382, 1597 ,78   ],
			[7,	1278, 3721 ,1067 ],
			[8, 1453, 3965 ,1251 ],
			[9,	1353, 3813 ,1060 ]
			]);

      var options = {
        chart: {
          title: 'Trand Line Of Common API  20 Jan - 28 Jan',
          subtitle: 'January February 2015'
        },
		legend: 'none',
        width: 900,
        height: 500,
		axes: {
          x: {
            0: {side: 'bottom'}
          }
        }
      };

      var chart = new google.charts.Line(document.getElementById('line_top_x'));

      chart.draw(data, options);
    }
</script>

  <div id="line_top_x"></div>
<meta http-equiv="refresh" content="20;URL=index8.php" >
	