#!/usr/local/bin/perl
#use strict;
use warnings;
use DBI;

##########################################################
#                                                        #
# FRANCO CARDINALI | FCARDI01 | BSc Final Year Project   #
# Application Performances and Statistics Tool   - APST  #
# SCRIPT: MasterResponsesCache.pl                        #
# This Script Will store Information about Errors Cache  #
# VERSION 1		17 February 2015                         #
##########################################################

#VARIABLES
################################################################################################################
                                                                                                               
#dbh CONTAINS DB CONNECTION CREDENTIALS                                                                        
my $dbh = DBI->connect( "dbi:Oracle:DB1", "fred", "fred" ) or die "Couldnt connect to database: $DBI::errstr"; 

#file IS THE LOCATION OF THE FILE WHERE THE FTP JOB WILL DROP THE LOGS READY TO BE PROCESSED                   
my $file ='C:\git\repo\finalProject\logs\responses\response.txt';

#StringError IS A STRING VARIABLE TO HELP ME WITH THE INSERT 
my $StringError='ERROR';

#those variable helps load into the DB the Cache Errors assinging a generic login  
my $login='None';
my $cid=0;
my $uid=0;

#line KEEPS THE COUNT OF HOW MANY RECORDS I HAVE INSERTED
my $line=0;
#################################################################################################################

# Printing On The Screen
print"Fetching Log File - Looking For Errors Cache\n\n";

# Opening File Streaming
open my $fh, "<", $file or die "Could not open $file: $!";

# Processing Each Line
# REGEXP will store Errors only
while (<$fh>) {
	my @fields = m{^
        (\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:[\d,]+)
        \s (WARN\s | ERROR) \s
        \[Retailer \scache \smanager \sdaemon \s[(]ThreadId:\s\d+[)]] 
        \s com.metapack.deliverymanager.(\S+) \s \(\S+\.java\:\d+\) \s
        - \s (.*) 
           $}x
	
	
	# Skipping Records That will not match the pattern
    or next;  
    
	
	# Storing USERID AND CLIENTID into Variables to use in SQL inserts
	my ( $DATETIME_EVENT, $LOG_LEVEL,  $SERVICE, $MESSAGE ) = @fields;

	
	# PREPARING INSERT
	my $sth = $dbh->prepare('INSERT INTO RESPONSE (DATETIME_EVENT,LOG_LEVEL,LOGONID,CLIENTID ,USERID ,SERVICE, MESSAGE)
	VALUES (?, ?, ?, ? ,?, ?,?)')
	or die "Couldn't execute statement: " . $dbh->errstr;
		
	# EXECUTING INSERT
	$sth->execute( $DATETIME_EVENT, $StringError, $login, $cid, $uid,$SERVICE ,$MESSAGE )
	or warn 'Execute failed: ' . $dbh->errstr;
	
	#Counter Variable For Inserted Rows
	$line= $line + 1;
}

# Printing On The Screen
print"Log File Scanned \n\nNow Disconnecting From DB \n\n";

# DISCONNECTING THE DB
$dbh->disconnect();

# Printing On The Screen
print"Potential Line/s  Inserted: $line\n ";