#!/usr/local/bin/perl
#use strict;
use warnings;
use DBI;

##########################################################
#                                                        #
# FRANCO CARDINALI | FCARDI01 | BSc Final Year Project   #
# Application Performances and Statistics Tool   - APST  #
# SCRIPT: MasterResponsesError.pl                        #
# This Script Will store Information about Errors Only   #
# VERSION 2.1	14 February 2015                         #
##########################################################

#VARIABLES
################################################################################################################
                                                                                                               
#dbh CONTAINS DB CONNECTION CREDENTIALS                                                                        
my $dbh = DBI->connect( "dbi:Oracle:DB1", "fred", "fred" ) or die "Couldnt connect to database: $DBI::errstr"; 

#file IS THE LOCATION OF THE FILE WHERE THE FTP JOB WILL DROP THE LOGS READY TO BE PROCESSED                   
my $file ='C:\git\repo\finalProject\logs\responses\response.txt';

#StringError IS A STRING VARIABLE TO HELP ME WITH THE INSERT 
my $StringError='ERROR';

#line KEEPS THE COUNT OF HOW MANY RECORDS I HAVE INSERTED
my $line=0;
#################################################################################################################

# Printing On The Screen
print"Fetching Log File - Looking For Errors\n\n";

# Opening File Streaming
open my $fh, "<", $file or die "Could not open $file: $!";

# Processing Each Line
# REGEXP will store Errors only
while (<$fh>) {
	my @fields = m{^
        (\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:[\d,]+)
        \s (INFO | INFO\s | ERROR | WARN\s) \s
        \[(?: SOAP | GUI )\s[(]User:["](\w+)",\sThreadId:\s\d+\)] 
        \s com.metapack.deliverymanager.(\S+) \s \(\S+\.java\:\d+\) \s
        - (?! \sRetailer | \sCreated | \s[[SQ] )
        \s (.*) 
           $}x
	
	
	# Skipping Records That will not match the pattern
    or next;  
    
    my $value  = "FATAL ERROR";
	my $value2 = "Error";
	my $value3 = "Unhandled error";
	my $value4 = "ERROR";
	
	if ( grep( /^$value$/, $fields[4] ) ) {
		$fields[4] = 'Error on The Label Template';
	}
	if ( grep( /^$value2$/, $fields[4] ) ) {
		$fields[4] = 'Error: com.metapack.deliverymanager.exceptions.InvalidDataException';
	}
	if ( grep( /^$value3$/, $fields[4] ) ) {
		$fields[4] = 'AxisFault: faultCode: {http://schemas.xmlsoap.org/soap/envelope/}Server.generalException';
	}
	if ( grep( /^$value4$/, $fields[4] ) ) {
		$fields[4] = 'Error on The Label Template';
	}
    

	
	# Storing USERID AND CLIENTID into Variables to use in SQL inserts
	my ( $DATETIME_EVENT, $LOG_LEVEL, $LOGONID, $SERVICE, $MESSAGE ) = @fields;
	
		# Brilliant: getting CLIENTID and USERID in the WHILE, reading each line and querying the CLIENT_USER table 
		my $sql = 'SELECT CLIENTID, USERID  FROM CLIENT_USER WHERE LOGONID  = ?';
		my $st = $dbh->prepare($sql);
		$st->execute("$LOGONID");
		my @row = $st->fetchrow_array;
	
	
	# PREPARING INSERT
	my $sth = $dbh->prepare('INSERT INTO RESPONSE (DATETIME_EVENT,LOG_LEVEL,LOGONID,CLIENTID ,USERID ,SERVICE, MESSAGE)
	VALUES (?, ?, ?, ? ,?, ?,?)')
	or die "Couldn't execute statement: " . $dbh->errstr;
		
	# EXECUTING INSERT
	# notes: $row[0] and  $row[1] are the values that correspond to CLIENTID and USERID
	# notes: the Variable $StringError is forced to insert the value 'ERROR'
	$sth->execute( $DATETIME_EVENT, $StringError, $LOGONID, $row[0], $row[1],$SERVICE ,$MESSAGE )
	or warn 'Execute failed: ' . $dbh->errstr;
	
	#Counter Variable For Inserted Rows
	$line= $line + 1;
}

# Printing On The Screen
print"Log File Scanned \n\nNow Disconnecting From DB \n\n";

# DISCONNECTING THE DB
$dbh->disconnect();

# Printing On The Screen
print"Potential Line/s  Inserted: $line\n ";