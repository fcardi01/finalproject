@echo off

echo Installing Cronjobs For Log management and DB Entries
SchTasks /Create /SC Daily /TN "Moving Log Files After FTP Transfer From Application Server" /TR "C:\git\repo\finalProject\cronjobs\scripts\MovingFile.bat" /ST 00:30
SchTasks /Create /SC Daily /TN "Inserting SOAP Requests into the Table Request via PERL" /TR "D:\Strawberry\perl\bin\perl.exe C:\git\repo\finalProject\cronjobs\scripts\MasterRequestSOAP.pl" /ST 00:35
SchTasks /Create /SC Daily /TN "Inserting GUI Requests into the Table Response via PERL" /TR "D:\Strawberry\perl\bin\perl.exe C:\git\repo\finalProject\cronjobs\scripts\MasterRequestGui.pl" /ST 00:35
SchTasks /Create /SC Daily /TN "Inserting Responses into the Table Response via PERL" /TR "D:\Strawberry\perl\bin\perl.exe C:\git\repo\finalProject\cronjobs\scripts\MasterResponses.pl" /ST 00:35
SchTasks /Create /SC Daily /TN "Inserting Errors App Responses into the Table Response via PERL" /TR "D:\Strawberry\perl\bin\perl.exe C:\git\repo\finalProject\cronjobs\scripts\MasterResponsesError.pl" /ST 00:35
SchTasks /Create /SC Daily /TN "Inserting Warning SQL Responses into the Table Response via PERL" /TR "D:\Strawberry\perl\bin\perl.exe C:\git\repo\finalProject\cronjobs\scripts\MasterResponsesWarn.pl" /ST 00:35
SchTasks /Create /SC Daily /TN "Inserting Errors Cache Responses into the Table Response via PERL" /TR "D:\Strawberry\perl\bin\perl.exe C:\git\repo\finalProject\cronjobs\scripts\MasterResponsesCache.pl" /ST 00:35
SchTasks /Create /SC Daily /TN "Inserting Proxy restarts into the Table Response via PERL" /TR "D:\Strawberry\perl\bin\perl.exe C:\git\repo\finalProject\cronjobs\scripts\MasterResponsesRestarts.pl" /ST 00:35
SchTasks /Create /SC Daily /TN "Remove Files After All The Daily DB Entries" /TR "C:\git\repo\finalProject\cronjobs\scripts\CleaningFile.bat" /ST 00:50
echo Cronjobs Installed