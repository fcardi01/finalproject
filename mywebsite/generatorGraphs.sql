/*

##########################################################
#                                                        #
# FRANCO CARDINALI | FCARDI01 | BSc Final Year Project   #
# Application Performances and Statistics Tool   - APST  #
# QUERIES FOR GOOGLE CHART                               #
# These SQL Scripts will generate the graph to be        #
# published   									         #
#                                                        #
##########################################################
*/

-- LIVE DATA

-- Request GUI Profile For all The dates --> http://mywebsite.localdomain/index.php
SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total from request Where log_level not in 'INFO' group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) order by 1

-- Request SOAP Profile For all The dates --> http://mywebsite.localdomain/index2.php
SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total from request Where log_level not in 'GUI' group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) order by 1

--Total Error Breakdown by API used --> http://mywebsite.localdomain/index3.php
select distinct(service) soap ,count(*) total from response where log_level='ERROR' group by service order by  2 desc;

-- Total Request for Logonid/user order by top hitter --> http://mywebsite.localdomain/index4.php
SELECT logonid, count(*) as total from request Where userid not in (0)  having count (*)> 8000 group by logonid order by 2 desc;

-- Error Breakdown For user --> http://mywebsite.localdomain/index5d.php
select distinct(SERVICE) as method, Count(*) as total from response where log_level ='ERROR' and logonid='itliveuser' group by SERVICE order by 2 desc

-- Error Breakdown For user --> http://mywebsite.localdomain/index5c.php
select distinct(SERVICE) as method, Count(*) as total from response where log_level ='ERROR' and logonid='citipostapi' group by SERVICE order by 2 desc

-- Error Breakdown For user --> http://mywebsite.localdomain/index5b.php
select distinct(SERVICE) as method, Count(*) as total from response where log_level ='ERROR' and logonid='boohhoorpapi' group by SERVICE order by 2 desc

-- Error Breakdown For user --> http://mywebsite.localdomain/index5.php
select distinct(SERVICE) as method, Count(*) as total from response where log_level ='ERROR' and logonid='motelcentralapi' group by SERVICE order by 2 desc

-- Error Breakdown For user -->http://mywebsite.localdomain/index6.php
select distinct(classmethod) api , count (*)as total from request Where logonid not in ('None')  having count (*)> 14000 group by classmethod order by 2 desc

-- Requests breakdown By Client and not User --> http://mywebsite.localdomain/index8.php
SELECT DISTINCT(c.NAME)as  Client , count(*) as total from request r, client c where  r.log_level not in 'GUI' and r.CLIENTID = c.CLIENTID  group by c.NAME order by 2 desc

-- GUI Request Profile - Average Hourly Breakdown for 20 days --> http://mywebsite.localdomain/index9.php
SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total from request where log_level in 'GUI' group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) order by 1;

-- SOAP Request Profile - Average Hourly Breakdown for 20 days --> http://mywebsite.localdomain/index10.php
SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total from request where log_level not in 'GUI' group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) order by 1;

-- Profile for Restarts Happened --> http://mywebsite.localdomain/index11.php
SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total from response Where message ='Proxy system started' group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) order by 1


-- STATIC DATA

-- Daily Breakdown of the Most used API inthe System --> http://mywebsite.localdomain/index7.php

SELECT 	
	DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day ,	
	classmethod, 
	count(*) as total 
from request 
where classmethod in 
		('ConsignmentSearchService.findConsignmentsByOrderReference',	
		'AllocationService.findDeliveryOptionsUsingBookingCode',	
		'ConsignmentService.createPaperworkForParcel')
group by 
	(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))),
	classmethod
order by 1 asc, 2;