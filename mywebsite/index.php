<?php

					ini_set('max_execution_time', 123456);

					$conn=oci_connect('fred','fred','localhost/DB1');

					If (!$conn)

					if (!$conn) {
					$e = oci_error();
					trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
					}

					$query= oci_parse($conn, "SELECT DISTINCT(TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS')))as day , count(*) as total from request Where log_level not in 'INFO' group by (TRUNC(to_date(SUBSTR(datetime_event,0,19), 'YYYY-MM-DD HH24:MI:SS'))) order by 1");
					oci_execute($query);

					$rows = array();
					$table = array();
					$table['cols'] = array(

					array('label' => 'day', 'type' => 'string'),
					array('label' => 'total', 'type' => 'number'),
					);

					$rows = array();
					while($r = oci_fetch_array($query, OCI_ASSOC+OCI_RETURN_NULLS)) {
					$temp = array();
					
					
					$temp[] = array('v' => (string) $r["DAY"]);
					
					$temp[] = array('v' => (int) $r["TOTAL"]);
					
					$rows[] = array('c' => $temp);
					}
					
					$table['rows'] = $rows;
					$jsonTable = json_encode($table);

					

					
				?>


					<!–Load the Ajax API–>

					<script type="text/javascript" src="https://www.google.com/jsapi"></script>
					<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
					<script type="text/javascript">


						  google.load("visualization", "1.1", {packages:["corechart"]});
						  google.setOnLoadCallback(drawChart);
						  function drawChart() {

						 
						  
					var data = new google.visualization.DataTable(<?=$jsonTable?>);
					  
					  var options = {
									   title: 'GUI Request Profile - Daily Aggregate 20 Jan - 2 Feb',
									  curveType: 'function',
									  legend: 'none',
									  lineWidth: 5,
									    series: {
												0: { color: '#6f9654' },
											  }
									};

					//To create a line chart
					var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
					chart.draw(data, options);

					}
					</script>



<!–this is the div that will hold the line chart chart–>
<div id="curve_chart" style="width: 1250px; height: 550px;"></div>
<meta http-equiv="refresh" content="20;URL=index2.php">